package fr.acceis.jpa.test.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connexion {
	private static Connection connect;
	
	private Connexion() {
		try {
			connect = DriverManager.getConnection("jdbc:hsqldb:data/basejpa", "sa",  "");
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	//Méthode qui va nous retourner notre instance et la créer si elle n'existe pas
	   public static Connection getInstance(){
	    if(connect == null){
	      new Connexion();
	    }
	    return connect;   
	  }  
	   
	   public static void shutdown() throws SQLException {
		   connect.close();
	   }
}
