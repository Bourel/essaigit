package fr.acceis.jpa.test.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestJdbc {

	public static void main(String[] args) throws Exception {
		Class.forName("org.hsqldb.jdbcDriver").newInstance();

		listerEtudiants();
		listerProfesseurs();
		listerSalles();
		cursusEtudiant("21002127");
		salleCours(67);
		listerCoursSalle("i57");
		listerEtudiantsCours(67);
		listerProfesseursCursus(10);
		//		listerProfesseursMatiere(2);
		//		listerProfsEtudiant("21002127");
		//		emploiDuTempsSalle("i52");
		//		emploiDuTempsEtudiant("21002128");
		//		emploiDuTempsProfesseur(55);
		//		renvoyer("21002128");
		Connexion.shutdown();
	}

	//	Liste les étudiants
	private static void listerEtudiants() throws SQLException {

		Connection connect = Connexion.getInstance();
		PreparedStatement stmt = connect.prepareStatement("SELECT * FROM Etudiant");
		ResultSet result = stmt.executeQuery();

		System.out.println("Liste des élèves");
		while (result.next()) {
			System.out.println("- " + result.getString("nom") + " " + result.getString("prenom") + " " + result.getString("numeroEtudiant"));
		}
		System.out.println("-------------");
		System.out.println();
		stmt.close();
	}

	//	Liste les professeurs
	private static void listerProfesseurs() throws SQLException {

		Connection connect = Connexion.getInstance();
		PreparedStatement stmt = connect.prepareStatement("SELECT * FROM Professeur");
		ResultSet result = stmt.executeQuery();

		System.out.println("Liste des professeurs");
		while (result.next()) {
			System.out.println("- " + result.getString("nom") + " " + result.getString("prenom"));
		}
		System.out.println("-------------");
		System.out.println();
		stmt.close();
	}

	//	Liste les salles
	private static void listerSalles() throws SQLException {
		Connection connect = Connexion.getInstance();
		PreparedStatement stmt = connect.prepareStatement("SELECT * FROM Salle");
		ResultSet result = stmt.executeQuery();

		System.out.println("Liste des salles");
		while (result.next()) {
			System.out.println("- " + result.getInt("id") + " " + result.getString("nom"));
		}
		System.out.println("-------------");
		System.out.println();
		stmt.close();
	}

	//	Affiche le nom du cursus d'un étudiant
	private static void cursusEtudiant(String numeroEtudiant) throws SQLException {
		Connection connect = Connexion.getInstance();
		String query = "SELECT nom FROM Cursus "
				+ "INNER JOIN Etudiant ON cursus.id=etudiant.cursus_id "
				+ "WHERE etudiant.numeroEtudiant=?";

		PreparedStatement stmt = connect.prepareStatement(query);
		stmt.setString(1, numeroEtudiant);
		ResultSet result = stmt.executeQuery();

		while (result.next()){
			System.out.println("Le cursus de l'élève n° " + numeroEtudiant + " est le " + result.getString("nom"));
		}
		System.out.println("-------------");
		System.out.println();
		stmt.close();
	}

	//	Affiche le nom de la salle dans laquelle a lieu un cours
	private static void salleCours(long idCours) throws SQLException {
		Connection connect = Connexion.getInstance();
		String query = "SELECT nom FROM Salle "
				+ "INNER JOIN Creneau ON salle.id=creneau.salle_id "
				+ "INNER JOIN Cours ON creneau.id=cours.creneau_id "
				+ "WHERE cours.id=?";

		PreparedStatement stmt = connect.prepareStatement(query);
		stmt.setLong(1, idCours);
		ResultSet result = stmt.executeQuery();

		while(result.next()) {
			System.out.println("La salle dans laquelle a lieu le cours " + idCours + " est la n° " + result.getString("nom"));
		}
		System.out.println("-------------");
		System.out.println();
		stmt.close();
	}

	//	Affiche le nom des cours ayant lieu dans une salle
	private static void listerCoursSalle(String nomSalle) throws SQLException {
		Connection connect = Connexion.getInstance();
		String query = "SELECT nom FROM Matiere "
				+ "INNER JOIN Cours ON matiere.id=cours.matiere_id "
				+ "INNER JOIN Creneau ON cours.id=creneau.cours_id "
				+ "INNER JOIN Salle ON creneau.salle_id=salle.id "
				+ "WHERE salle.nom=?";

		PreparedStatement stmt = connect.prepareStatement(query);
		stmt.setString(1, nomSalle);
		ResultSet result = stmt.executeQuery();

		System.out.println("Les cours de la salle " + nomSalle + " sont : ");
		while(result.next()) {
			System.out.println("\t" + result.getString("nom"));
		}
		System.out.println("-------------");
		System.out.println();
		stmt.close();
	}

	//	Affiche le nom des étudiants qui assistent à un cours
	private static void listerEtudiantsCours(long idCours) throws SQLException {
		Connection connect = Connexion.getInstance();
		String query = "SELECT nom, prenom, numeroEtudiant FROM Etudiant "
				+ "INNER JOIN Cursus ON etudiant.Cursus_id=cursus.id "
				+ "INNER JOIN Cursus_Matiere ON cursus.id=Cursus_Matiere.cursus_id "
				+ "INNER JOIN Matiere ON matiere.id=Cursus_Matiere.matieres_id "
				+ "INNER JOIN Cours ON cours.matiere_id=matiere.id "
				+ "WHERE cours.id=?";

		PreparedStatement stmt = connect.prepareStatement(query);
		stmt.setLong(1, idCours);
		ResultSet result = stmt.executeQuery();

		System.out.println("Les étudiants qui assistent au cours " + idCours + " sont : ");
		while(result.next()) {
			System.out.println("\t" + result.getString("nom") + " " + result.getString("prenom") + " " + result.getString("numeroEtudiant"));
		}
		System.out.println("-------------");
		System.out.println();
		stmt.close();
	}

	//	Affiche le nom des professeurss qui enseignent dans un cursus
	private static void listerProfesseursCursus(long idCursus) throws SQLException {
		Connection connect = Connexion.getInstance();
		String query = "SELECT nom, prenom FROM Professeur "
				+ "INNER JOIN Cours ON professeur.id=Cours_Professeur.cours_id "
				+ "INNER JOIN Cours_Professeur ON professeur.cours_id=cours.id "
				+ "INNER JOIN Matiere ON cours.matiere_id=matiere.id "
				+ "INNER JOIN Cursus_Matiere ON matiere.id=Cursus_Matiere.matieres_id "
				+ "INNER JOIN Cursus ON cursus.id_id=Cursus_Matiere.id "
				+ "WHERE cursus.id=?";
		
		PreparedStatement stmt = connect.prepareStatement(query);
		stmt.setLong(1, idCursus);
		ResultSet result = stmt.executeQuery();

		System.out.println("Les professeurs qui enseignent le cursus n° " + idCursus + " sont : ");
		while(result.next()) {
			System.out.println("\t" + result.getString("nom") + " " + result.getString("prenom"));
		}
		System.out.println("-------------");
		System.out.println();
		stmt.close();
	}

	//	Affiche le nom des professeurs qui enseignent une matière
	private static void listerProfesseursMatiere(long idMatiere) {

	}

	//	Affiche des profs qui enseignent à un étudiant
	private static void listerProfsEtudiant(String numeroEtudiant) {

	}






	//	Affiche l'emploi du temps d'une salle
	private static void emploiDuTempsSalle(String nomSalle) {

	}

	//	Affiche l'emploi du temps d'un étudiant
	private static void emploiDuTempsEtudiant(String numeroEtudiant) {

	}

	//	Affiche l'emploi du temps d'un professeur
	private static void emploiDuTempsProfesseur(long idProfesseur) {

	}

	// Renvoie un étudiant
	private static void renvoyer(String numeroEtudiant) {

	}

}
