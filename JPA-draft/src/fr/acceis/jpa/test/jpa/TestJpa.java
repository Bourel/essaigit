package fr.acceis.jpa.test.jpa;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.acceis.jpa.test.jpa.HibernateUtil;
import fr.acceis.jpa.test.jpa.model.Cours;
import fr.acceis.jpa.test.jpa.model.Cursus;
import fr.acceis.jpa.test.jpa.model.Etudiant;
import fr.acceis.jpa.test.jpa.model.Matiere;
import fr.acceis.jpa.test.jpa.model.Professeur;
import fr.acceis.jpa.test.jpa.model.Salle;


public class TestJpa {

	public static void main(String[] args) throws Exception {

		//listerEtudiants();
		//listerProfesseurs();
		//listerSalles();
		//cursusEtudiant("21002127");
		//salleCours(67);
		//listerCoursSalle("i57");
		//listerEtudiantsCours(67);
		//listerProfesseursCursus(10);
		listerProfesseursMatiere(2);
		//		listerProfsEtudiant("21002127");
		//		emploiDuTempsSalle("i52");
		//		emploiDuTempsEtudiant("21002128");
		//		emploiDuTempsProfesseur(55);
		//		renvoyer("21002128");
	}

	//	Liste les étudiants
	private static void listerEtudiants() {
		Session session = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Etudiant> criteria = criteriaBuilder.createQuery(Etudiant.class);
		Root<Etudiant> root = criteria.from(Etudiant.class);
		criteria.select(root);
		Query<Etudiant> query = session.createQuery(criteria);
		List<Etudiant> list = query.getResultList();
		System.out.println("-------------------------");
		for (Etudiant etudiant : list) {
			System.out.println(etudiant.getPrenom() + " " + etudiant.getNom() + " " + etudiant.getNumeroEtudiant() + " inscrit en " + etudiant.getCursus().getNom()) ;
			System.out.println("Il possède " + etudiant.getCursus().getEtudiant().size() + " camarades");
		}
		System.out.println("-------------------------");
	}

	//	Liste les professeurs
	private static void listerProfesseurs() {
		Session session = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Professeur> criteria = criteriaBuilder.createQuery(Professeur.class); 
		Root<Professeur> root = criteria.from(Professeur.class);
		criteria.select(root);
		Query<Professeur> query = session.createQuery(criteria);
		List<Professeur> list = query.getResultList();
		System.out.println("-------------------------");
		for (Professeur professeur : list) {
			System.out.println(professeur.getPrenom() + " " + professeur.getNom() + " " + professeur.getId());
		}
		System.out.println("-------------------------");
	}

	//	Liste les salles
	private static void listerSalles() {
		Session session = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Salle> criteria = criteriaBuilder.createQuery(Salle.class);
		Root<Salle> root = criteria.from(Salle.class);
		criteria.select(root);
		Query<Salle> query = session.createQuery(criteria);
		List<Salle> list = query.getResultList();
		System.out.println("-------------------------");
		for (Salle salle : list) {
			System.out.println(salle.getNom() + " " + salle.getId());
		}
		System.out.println("-------------------------");
	}

	//	Affiche le nom du cursus d'un étudiant
	private static void cursusEtudiant(String numeroEtudiant) {
		Session session = HibernateUtil.getInstance();
		Etudiant etudiant = session.load(Etudiant.class, numeroEtudiant);
		System.out.println("-------------------------");
		System.out.println("L'étudiant n°" + numeroEtudiant + " suit le cursus " + etudiant.getCursus().getNom());
		System.out.println("-------------------------");
	}

	//	Affiche le nom de la salle dans laquelle a lieu un cours
	private static void salleCours(long idCours) {
		Session session = HibernateUtil.getInstance();
		Cours cours = session.load(Cours.class, idCours);
		System.out.println("-------------------------");
		System.out.println("La salle dans laquelle a lieu le cours n°" + idCours + " est la n° " + cours.getCreneau().getSalle().getNom());
		System.out.println("-------------------------");
	}

	//	Affiche le nom des cours ayant lieu dans une salle
	private static void listerCoursSalle(String nomSalle) {
		Session session= HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Cours> criteria = criteriaBuilder.createQuery(Cours.class);
		Root<Cours> root = criteria.from(Cours.class);
		criteria.select(root);
		Query<Cours> query = session.createQuery(criteria);
		List<Cours> list = query.getResultList();
		System.out.println("-------------------------");
		for(Cours cours : list) {
			if (cours.getCreneau().getSalle().getNom().equals(nomSalle)) {
				System.out.println(cours.getMatiere().getNom());
			}	
		}
		System.out.println("-------------------------");
	}

	//	Affiche le nom des étudiants qui assistent à un cours
	private static void listerEtudiantsCours(long idCours) {
		Session session= HibernateUtil.getInstance();
		Cours cours = session.load(Cours.class, idCours);
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Etudiant> criteria = criteriaBuilder.createQuery(Etudiant.class);
		Root<Etudiant> root = criteria.from(Etudiant.class);
		criteria.select(root);
		Query<Etudiant> query = session.createQuery(criteria);
		List<Etudiant> list = query.getResultList();
		System.out.println("-------------------------");
		for(Etudiant etudiant : list) {
			if(etudiant.getCursus().getNom().equals(cours.getMatiere().getCursus().iterator().next().getNom())) {
				System.out.println(etudiant.getNom());
			}	
		}
		System.out.println("-------------------------");
	}

	//	Affiche le nom des professeurs qui enseignent dans un cursus
	private static void listerProfesseursCursus(long idCursus) {
		Session session = HibernateUtil.getInstance();
		Cursus cursus = session.load(Cursus.class, idCursus);

		Set<Professeur> professeurs = new HashSet<Professeur>();
		for (Matiere matiere : cursus.getMatiere()) {
			for (Cours cours : matiere.getCours()) {
				for (Professeur professeur : cours.getProfesseur()) {
					professeurs.add(professeur);
				}
			}
		}
		System.out.println("* Les professeurs suivants enseignent au sein du cursus " + cursus.getNom() + " :");
		for (Professeur professeur : professeurs) {
			System.out.println(professeur.getNom());
		}
		System.out.println("* Fin de la liste des professeurs");
		System.out.println();

	}

		//	Affiche le nom des professeurs qui enseignent une matière
		private static void listerProfesseursMatiere(long idMatiere) {
			Session session = HibernateUtil.getInstance();
			Matiere matiere = session.load(Matiere.class, idMatiere);


			Set<Professeur> professeurs = new HashSet<Professeur>();
			for (Cours cours : matiere.getCours()) {
				for (Professeur professeur : cours.getProfesseur()) {
					professeurs.add(professeur);
				}
			}

			System.out.println("* Les professeurs suivants enseignent la matière \"" + matiere.getNom() + "\" :");
			for (Professeur professeur : professeurs) {
				System.out.println(professeur.getNom());
			}
			System.out.println("* Fin de la liste des professeurs");
			System.out.println();

		}

		//	Affiche des profs qui enseignent à un étudiant
		private static void listerProfsEtudiant(String numeroEtudiant) {

		}






		//	Affiche l'emploi du temps d'une salle
		private static void emploiDuTempsSalle(String nomSalle) {

		}

		//	Affiche l'emploi du temps d'un étudiant
		private static void emploiDuTempsEtudiant(String numeroEtudiant) {

		}

		//	Affiche l'emploi du temps d'un professeur
		private static void emploiDuTempsProfesseur(long idProfesseur) {

		}

		// Renvoie un étudiant
		private static void renvoyer(String numeroEtudiant) {

		}

	}
