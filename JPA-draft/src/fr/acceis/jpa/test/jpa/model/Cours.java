package fr.acceis.jpa.test.jpa.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Cours")
public class Cours {
	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne
	private Matiere matiere;
	@ManyToMany()
	@JoinTable(name="Cours_Professeur",
		joinColumns=@JoinColumn(name="cours_id"),
		inverseJoinColumns=@JoinColumn(name="professeurs_id"))
	private List<Professeur> professeur;
	@OneToOne
	private Creneau creneau;
	
	public Matiere getMatiere() {
		return matiere;
	}
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}
	public List<Professeur> getProfesseur() {
		return professeur;
	}
	public void setProfesseur(List<Professeur> professeur) {
		this.professeur = professeur;
	}
	public Creneau getCreneau() {
		return creneau;
	}
	public void setCreneau(Creneau creneau) {
		this.creneau = creneau;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}
