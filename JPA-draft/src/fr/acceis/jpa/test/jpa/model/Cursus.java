package fr.acceis.jpa.test.jpa.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Cursus")
public class Cursus {	
	@Id
	@GeneratedValue
	private Long id;
	private String nom;
	@OneToMany(mappedBy="cursus")
	private List<Etudiant> etudiant ;
	@ManyToMany()
	@JoinTable(name="Cursus_Matiere",
		joinColumns=@JoinColumn(name="Cursus_id"),
		inverseJoinColumns=@JoinColumn(name="matieres_id"))
	private List<Matiere> matiere;
	
	public List<Matiere> getMatiere() {
		return matiere;
	}
	public void setMatiere(List<Matiere> matiere) {
		this.matiere = matiere;
	}
	public List<Etudiant> getEtudiant() {
		return etudiant;
	}
	public void setEtudiant(List<Etudiant> etudiant) {
		this.etudiant = etudiant;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}	
}
