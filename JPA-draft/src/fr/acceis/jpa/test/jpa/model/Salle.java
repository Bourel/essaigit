package fr.acceis.jpa.test.jpa.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Salle")
public class Salle {
	@Id
	private Long id;
	private String nom;
	@OneToMany(mappedBy="salle")
	private List<Creneau> creneau;

	public List<Creneau> getCreneau() {
		return creneau;
	}
	public void setCreneau(List<Creneau> creneau) {
		this.creneau = creneau;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
}

