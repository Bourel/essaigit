package fr.acceis.jpa.test.jpa.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Matiere")
public class Matiere {
	@Id
	@GeneratedValue
	private Long id;
	private String nom;
	@ManyToMany()
	@JoinTable(name="Cursus_Matiere",
		joinColumns=@JoinColumn(name="matieres_id"),
		inverseJoinColumns=@JoinColumn(name="Cursus_id"))
	private List<Cursus> cursus;
	@OneToMany(mappedBy="matiere")
	private List<Cours> cours;
	
	public List<Cours> getCours() {
		return cours;
	}
	public void setCours(List<Cours> cours) {
		this.cours = cours;
	}
	public List<Cursus> getCursus() {
		return cursus;
	}
	public void setCursus(List<Cursus> cursus) {
		this.cursus = cursus;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
}
