package fr.acceis.jpa.test.jpa.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="Professeur")
public class Professeur {
	@Id
	private String id;
	private String nom;
	private String prenom;
	
	@ManyToMany()
	@JoinTable(name="Cours_Professeur",
		joinColumns=@JoinColumn(name="professeurs_id"),
		inverseJoinColumns=@JoinColumn(name="cours_id"))
	private List<Cours> cours;
	
	public List<Cours> getCours() {
		return cours;
	}
	public void setCours(List<Cours> cours) {
		this.cours = cours;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
}
