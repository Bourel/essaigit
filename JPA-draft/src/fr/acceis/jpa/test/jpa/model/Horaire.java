package fr.acceis.jpa.test.jpa.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Horaire")
public class Horaire {
	@Id
	@GeneratedValue
	private Long id;
	private Date debut;
	private Date fin;
	
	@OneToMany(mappedBy="horaire")
	private List<Creneau> creneau;
	
	public List<Creneau> getCreneau() {
		return creneau;
	}
	public void setCreneau(List<Creneau> creneau) {
		this.creneau = creneau;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDebut() {
		return debut;
	}
	public void setDebut(Date debut) {
		this.debut = debut;
	}
	public Date getFin() {
		return fin;
	}
	public void setFin(Date fin) {
		this.fin = fin;
	}
	
	
	
}
