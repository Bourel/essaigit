package jeuDemineur;

import java.util.Random;
import java.util.Scanner;

public class Demineur {
	private int [][] grille;
	private boolean [][] ouvertes;
	private int largeur = 0;
	private int longueur = 0;
	private int nbBombes = 0;
	private boolean partieEnCours = true;
	private boolean victoire = false;
	private int compteur = 0;


	public static void main(String[] args) {
		boolean nouvellePartie = true;
		while(nouvellePartie == true) {
			System.out.println("Bienvenue dans cette partie de d�mineur");
			Scanner scanner = new Scanner(System.in);
			System.out.println("Nombre de lignes ?");
			int nombreCasesLigne = scanner.nextInt();
			System.out.println("Nombre de colonnes ?");
			int nombreCasesColonne = scanner.nextInt();
			System.out.println("Nombre de bombes ?");
			int nombreBombes = scanner.nextInt();
			Demineur demineur = new Demineur(nombreCasesLigne, nombreCasesColonne, nombreBombes);
			System.out.println("Voulez-vous faire une nouvelle partie ? O/N ");
			scanner.nextLine();
			String k = scanner.nextLine();
			if(k.equals("N") || k.equals("n")) {
				nouvellePartie = false;
			} 
		}
		System.out.println("FIN");

	}

	// M�thode pour cr�er le tableau du d�mineur
	public Demineur(int largeur, int longueur, int nbBombes) {
		grille = new int[largeur][longueur];
		ouvertes = new boolean[largeur][longueur];
		this.largeur = largeur;
		this.longueur = longueur;
		this.nbBombes = nbBombes;
		placerBombes();
		afficher();
		jouer();
	}

	// M�thode permettant de placer les bombes dans le tableau
	private void placerBombes() {
		int nombreBombesPlacees = 0;
		while (nombreBombesPlacees < nbBombes) {
			int i = new Random().nextInt(largeur);
			int j = new Random().nextInt(longueur);
			if (grille[i][j] != -1) {
				grille[i][j] = -1;
				nombreBombesPlacees ++;
				for(int a = i-1; a <= i+1; a++) {
					for(int b = j-1; b <= j+1; b++) {
						if (a >= 0 && b >= 0 && a < largeur && b < longueur && grille[a][b] != -1) {
							grille[a][b]++;
						}
					}
				}
			}
		}
	}

	// M�thode pour afficher le tableau dans la console
	private void afficher() {
		System.out.println();
		System.out.println("-----------------------------");
		for (int i = 0; i < ouvertes.length; i++) {
			for (int j = 0; j < ouvertes[i].length; j++) {
				if(ouvertes[i][j] == true) {
					System.out.print(grille[i][j] + "\t");
				} else {
					System.out.print("[" + i + "." + j + "]" + "\t");
				}
			}
			System.out.println();
		}
		System.out.println("-----------------------------");
		System.out.println();
	}

	//M�thode pour rentrer les 2 chiffres � jouer
	private void jouer() {
		int i = 0;
		int j = 0;
		Scanner scanner = new Scanner(System.in);
		while (partieEnCours == true) {
			System.out.println("Quel num�ro de ligne ?");
			i = scanner.nextInt();
			while (i < 0 || i > (largeur-1)) {
				if (i < 0 || i > (largeur-1)) {
					System.out.println("Le num�ro que vous avez entr� est incorrect. Entrez un numero entre 0 et " + (largeur-1));
				}
				System.out.println("Quel num�ro de ligne ?");
				i = scanner.nextInt();
			}
			System.out.println("Quel num�ro de colonne ?");
			j = scanner.nextInt();
			while (j < 0 || j > (longueur-1)) {
				if (j < 0 || j > (longueur-1)) {
					System.out.println("Le num�ro que vous avez entr� est incorrect. Entrez un numero entre 0 et " + (longueur-1)) ;
				}
				System.out.println("Quel num�ro de colonne ?");
				j = scanner.nextInt();
			}
			ouvrir(i, j);
			compteur++;
			resultat(i, j);	
		}
	}

	//M�thode pour ouvrir la case dans le tableau en fonction des 2 chiffres rentr�s
	private void ouvrir (int i, int j) {
		ouvertes[i][j] = true;
		if(grille[i][j] == -1) {
			return;
		} else {
			if(grille[i][j] == 0) {
				for(int a = i-1; a <= i+1; a++) {
					for(int b = j-1; b <= j+1; b++) {
						if (a >= 0 && b >= 0 && a < largeur && b < longueur && grille[a][b] != -1 && ouvertes[a][b] == false) {
							ouvrir(a,b);		
						}
					}
				}
			}
		}
	}
	//M�thode qui d�termine une victoire ou une d�faite
	private void resultat(int i, int j) {
		if(grille[i][j] == -1) {
			for (int a = 0; a < grille.length; a++) {
				for (int b = 0; b < grille[a].length; b++) {
					if(grille[a][b] == -1) {
						ouvertes[a][b] = true;
					}	
				}
			}
			afficher();
			System.out.println("BOOM - LOOSER");
			System.out.println("Il vous a fallu " + compteur + " coups pour perdre. C'est vraiment pas brillant.");
			partieEnCours = false;
			return;
		}
		for (int k = 0; k < ouvertes.length; k++) {
			for (int l = 0; l < ouvertes[k].length; l++) {
				if(grille[k][l] != -1 && ouvertes[k][l] == true) {
					victoire = true;
				} else if (grille[k][l] != -1 && ouvertes[k][l] == false)  {
					victoire = false;
					afficher();
					return;
				}
			}
		}
		if(victoire == true) {
			System.out.println("Bravo, vous avez gagn�");
			System.out.println("Vous avez utilis� : " + compteur + " coups");
			partieEnCours = false;
		}
	}
}



